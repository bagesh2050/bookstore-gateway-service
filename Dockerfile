# Create an Image
FROM openjdk:8-jre-alpine
VOLUME /tmp

WORKDIR /opt/application/gateway
ADD target/*.jar app.jar
EXPOSE 7000
ENTRYPOINT ["java","-Dspring.profiles.active=prod","-jar","/opt/application/gateway/app.jar"]